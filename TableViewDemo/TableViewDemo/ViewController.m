//
//  ViewController.m
//  TableViewDemo
//
//  Created by 林浩智 on 2015/11/27.
//  Copyright © 2015年 林浩智. All rights reserved.
//

#import "ViewController.h"

#define CONTENT_OFFSET_TOP 200.0f

@interface ViewController ()<UITableViewDataSource,UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerViewHeight;
@end

@implementation ViewController {
    NSMutableArray *_dataSource;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self initData];
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initData {
    _dataSource = [NSMutableArray array];
    
    for (NSInteger i = 1; i <= 10; i++) {
        [_dataSource addObject:@(i)];
    }
}

- (void)initView {
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.tableView.contentInset = UIEdgeInsetsMake(CONTENT_OFFSET_TOP, 0, 0, 0);
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(CONTENT_OFFSET_TOP, 0, 0, 0);
//    [self.tableView setContentOffset:CGPointMake(0.0f, -CONTENT_OFFSET_TOP) animated:YES];
}

#pragma mark - UITableViewDataSource methdos
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@",_dataSource[indexPath.row]];
    
    return cell;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat scrollY = scrollView.contentOffset.y;
    CGFloat changeY = MIN(CONTENT_OFFSET_TOP, MAX(0, -scrollY));
    
    self.headerViewHeight.constant = MAX(0, -scrollY);
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(MAX(0, -scrollY), 0, 0, 0);
    self.tableView.contentInset = UIEdgeInsetsMake(changeY, 0, 0, 0);
}

@end
